package com.xzbd.mail.utils.mailContentUtil;

import com.xzbd.mail.utils.excelUtils.ExcelProperty;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;
import java.io.File;
import java.io.UnsupportedEncodingException;

public class ExcelEmailContentItem extends AbstractEmailContentItem {
    private ExcelProperty excel;

    public ExcelProperty getExcel() {
        return excel;
    }

    public void setExcel(ExcelProperty excel) {
        this.excel = excel;
    }

    @Override
    public BodyPart getMailBodyPart() throws MessagingException, UnsupportedEncodingException {
        BodyPart bodyPart = new MimeBodyPart();
        File originFile = new File(this.getExcel().getPath());
        DataHandler dh2 = new DataHandler(new FileDataSource(originFile)); // 读取本地文件
        bodyPart.setDataHandler(dh2); // 将附件数据添加到“节点”
        bodyPart.setFileName(MimeUtility.encodeText(originFile.getName()));
        return bodyPart;
    }
}
