package com.xzbd.mail.exceptions;

public class EmailException extends RuntimeException{

    private static final long serialVersionUID = 6213857434826870410L;
    
    public EmailException(String msg) {
        super(msg);
    }

}
